import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'firebase-angular';

  constructor(private apiService: ApiService)
  {
    
  }

  ngOnInit() {
    this.apiService.hello().subscribe((response) => {
      this.title = response.message;
    },
    error => console.log(error));
  }
}
